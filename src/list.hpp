#ifndef __LIST__
#define __LIST__

#include "node.hpp"

class list{
private:
  node* first_;
  node* last_;

public:
  // CONSTRUCTOR Y DESTRUCTOR
  list();
  ~list();

  // ACCESO Y MODIFICACION
  node* get_first();
  node* get_last();
  
  void push_front(DTYPE n_data);
  void push_back(DTYPE n_data);
  DTYPE pop_front();
  DTYPE pop_back();
  
  void erase(int pos);
  void insert(int pos,DTYPE data);
  bool empty();

  // SOBRECARGA DE OPERADORES
  friend ostream& operator<<(ostream& os, list&);
};

#endif