#include "node.hpp"

//////////////////////////////////////////
// CONSTRUCTOR
/////////////////////////////////////////
node::node(DTYPE data):
	data_(data),
	next_(NULL){}

//////////////////////////////////////////
// DESSTRUCTOR
/////////////////////////////////////////
node::~node(void){}

//////////////////////////////////////////
// METODOS DE ACCESO
/////////////////////////////////////////
DTYPE node::get_data(){
	return data_;}

void node::set_data(DTYPE& data){
	data_ = data; }


node* node::get_next(){	
	return next_;}

void node::set_next(node* next){
	next_ = next;}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
ostream& operator<<(ostream& os, node& n){
	os << n.get_data() << " ";
	return os;
}