#ifndef __STACK__
#define __STACK__

#include "list.hpp"

class stack : public list{
private:

public:
    //CONSTRUCTOR Y DESTRUCTOR
    stack();
    ~stack();

    //MÉTODOS DE MODIFICACIÓN
    void push(DTYPE n_data);
    DTYPE pop();
	
    /*/////////////////////////////////////////
    // METODOS HEREDADOS
    /////////////////////////////////////////
    
    // ACCESO Y MODIFICACION
    node* get_first();
    node* get_last();

    void push_front(DTYPE n_data);
    void push_back(DTYPE n_data);
    DTYPE pop_front();
    DTYPE pop_back();

    void erase(int pos);
    void insert(int pos,DTYPE data);
    bool empty();

    // SOBRECARGA DE OPERADORES
    friend ostream& operator<<(ostream& os, list&);
    
    */////////////////////////////////////////////
};

#endif