#include "vector.hpp"

//////////////////////////////////////////
// CONSTRUCTOR
/////////////////////////////////////////
vector::vector(const int lenght):
    first_(0),
    last_(lenght-1)
{
    v_ = new DTYPE[lenght];
    init(0);
}
//////////////////////////////////////////
// CONSTRUCTOR DE COPIA
/////////////////////////////////////////
vector::vector(vector& v){
    v_ = new DTYPE[v.size()];
    first_ = v.get_first();    last_ = v.get_last();
    for(int i=first_; i<=last_; i++)
	v_[i] = v[i];
}

//////////////////////////////////////////
// DESTRUCTOR
/////////////////////////////////////////
vector::~vector(){
	delete v_;
}


//////////////////////////////////////////
// ACCESO Y MODIFICACION
/////////////////////////////////////////
void vector::init(const DTYPE& data){
    for(int i = get_first(); i <= get_last(); i++)
	v_[i] = data;
}

int vector::get_first(){
    return first_;
}

void vector::set_first(int first){
    first_ = first;
}

int vector::get_last(){
    return last_;
}

int vector::size(){
    return (last_ - first_ + 1); 
}

//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
DTYPE& vector::operator[](const int pos){
    return v_[pos-first_];
}

vector& vector::operator=(vector& v){
    if(size() != v.size()){
	delete v_;
	v_ = new DTYPE[v.size()];
    }
    first_ = v.get_first();    last_ = v.get_last();
    for(int i=first_; i<=last_; i++)
	v_[i] = v[i];
    
    return *this;
}

ostream& operator<<(ostream& os, vector v){
    os << "[";
    for(int i=v.get_first(); i<v.get_last(); i++)
	os << v[i] << "," ;
    os << v[v.get_last()] << "]";
    return os;
}
	

