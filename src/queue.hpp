#ifndef __QUEUE__
#define __QUEUE__


#include "list.hpp"

class queue : public list{
private:
public:
    
    queue();
    ~queue();

    void push(DTYPE n_data);

    DTYPE pop();
	
    /*/////////////////////////////////////////
    // METODOS HEREDADOS
    /////////////////////////////////////////
    
    // ACCESO Y MODIFICACION
    node* get_first();
    node* get_last();

    void push_front(DTYPE n_data);
    void push_back(DTYPE n_data);
    DTYPE pop_front();
    DTYPE pop_back();

    void erase(int pos);
    void insert(int pos,DTYPE data);
    bool empty();

    // SOBRECARGA DE OPERADORES
    friend ostream& operator<<(ostream& os, list&);
    
    */////////////////////////////////////////////
};

#endif