#include "vector.hpp"
#include "list.hpp"
#include "queue.hpp"
#include "stack.hpp"

#include <iostream>
using namespace std;

int main(){

	// PRUEBAS VECTOR
	cout << "\n\n--- PRUEBAS VECTOR ---" << endl;
	vector v(10);
	for(int i=0; i<10; i++){
		v[i]=i;
		cout << v << endl;
	}

	//PRUEBAS LISTA
	cout << "\n\n--- PRUEBAS LISTA ---" << endl;
	cout << "\n--- LISTA1 PUSH/POP FRONT---" << endl;
	list l1;
	for(int i=0; i<10; i++){
		l1.push_front(i);
		cout << l1 << endl;
	}
	for(int i=0; i<10; i++){
		l1.pop_front();
		cout << l1 << endl;
	}

	cout << "\n--- LISTA2 PUSH/POP BACK---" << endl;
	list l2;
	for(int i=0; i<10; i++){
		l2.push_back(i);
		cout << l2 << endl;
	}
	for(int i=0; i<10; i++){
		l2.pop_back();
		cout << l2 << endl;
	}

	//PRUEBAS COLA
	cout << "\n\n--- PRUEBAS COLA ---" << endl;
	queue q;
	for(int i=0; i<10; i++){
		q.push(i);
		cout << q << endl;
	}
	for(int i=0; i<10; i++){
		q.pop();
		cout << q << endl;
	}

	//PRUEBAS PILA
	cout << "\n\n--- PRUEBAS PILA ---" << endl;
	stack s;
	for(int i=0; i<10; i++){
		s.push(i);
		cout << s << endl;
	}
	for(int i=0; i<10; i++){
		s.pop();
		cout << s << endl;
	}

	return 0;
}