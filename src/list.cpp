#include "list.hpp"

//////////////////////////////////////////
// CONSTRUCTOR
/////////////////////////////////////////
list::list():
	first_(NULL),
	last_(NULL){}

	
//////////////////////////////////////////
// DESTRUCTOR
/////////////////////////////////////////
list::~list(){}


//////////////////////////////////////////
// ACCESO
/////////////////////////////////////////
node* list::get_first(){
    return first_;
}
node* list::get_last(){
    return last_;
}

//////////////////////////////////////////
// PUSH
/////////////////////////////////////////
void list::push_front(DTYPE n_data){
	node* aux = new node(n_data);

	if(empty())	first_ = last_ = aux;
	else{
		aux -> set_next(first_);
		first_ = aux;
	}
	
}

void list::push_back(DTYPE n_data){
	node* aux = new node(n_data);

	if(empty())	first_ = last_ = aux;
	else{
		last_ -> set_next(aux);
		last_ = aux;
	}
}


//////////////////////////////////////////
// POP
/////////////////////////////////////////
DTYPE list::pop_front(){
	node* aux = first_;

	if(first_ == last_)		first_ = last_ = NULL;
	else					first_ = first_ -> get_next();

	DTYPE r_data = aux -> get_data();
	delete aux;
	return r_data;
}

DTYPE list::pop_back(){
	node* aux = first_;	
	if(first_ == last_)		first_ = last_ = NULL;
	else{
		while(aux -> get_next() != last_)
			aux = aux -> get_next();
		last_ = aux;
		aux = last_ -> get_next();
		last_ -> set_next(NULL);
	}

	DTYPE r_data = aux -> get_data();
	delete aux;
	return r_data;
		
}


//////////////////////////////////////////
// ELIMINACION
/////////////////////////////////////////
void list::erase(int pos){
	node* aux = first_;
	if(empty()){
		first_ = aux;
		last_ = aux;
	}
	else{
		if (pos == 0)		first_ = (aux -> get_next());
		else{
			node* a_node = aux;

			for(int i=0; i < pos-1; i++) a_node = a_node -> get_next();

			aux = a_node -> get_next();
			a_node -> set_next(a_node -> get_next() -> get_next());
		}
		delete aux;
	}
	
}



//////////////////////////////////////////
// INSERCION
/////////////////////////////////////////

void list::insert(int pos,DTYPE data){
	node* aux = new node(data);
	if (pos == 0){
		aux -> set_next(first_);
		first_ = aux;
	}
	else{
		int i=0;
		node* p_node = first_;
		while(i <= pos-1){
			if(i == pos-1){
				aux -> set_next(p_node -> get_next());
				p_node -> set_next(aux);
			}
		}
	}
}

//////////////////////////////////////////
// CONSULTA
/////////////////////////////////////////
bool list::empty(){
	return ((first_ == NULL) && (last_ == NULL))? true : false;
}


//////////////////////////////////////////
// SOBRECARGA DE OPERADORES
/////////////////////////////////////////
ostream& operator<<(ostream& os, list& l){
	if(!l.empty()){
		node* aux = l.get_first();
	    while(aux -> get_next() != NULL){
	        os << *aux << "-> ";
	        aux = aux -> get_next();
	    }

	    os << *aux;
	}
    return os;
}