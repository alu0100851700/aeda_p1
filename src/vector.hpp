#ifndef __VECTOR__
#define __VECTOR__


#include <cstdio>
#include <iostream>
using namespace std;

typedef int DTYPE;

class vector{
private:
    DTYPE* v_;
    int first_;
    int last_;

public:
    // CONSTRUCTOR, CONSTRUCTOR DE COPIA Y DESTRUCTOR
    vector(const int lenght);
    vector(vector& v);
    ~vector();

    // ACCESO Y MODIFICACION
    void init(const DTYPE& data);
    int get_first();
    int get_last();
    void set_first(int first);
    int size();
    
    //SOBRECARGA DE OPERADORES
    DTYPE& operator[](const int pos);
    vector& operator=(vector& v);
    friend ostream& operator<<(ostream& os, vector);

};

#endif