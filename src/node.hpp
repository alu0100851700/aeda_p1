#ifndef __NODE__
#define __NODE__

#include <cstdio>
#include <iostream>
using namespace std;

typedef int DTYPE;

class node{
private:
  DTYPE data_;
  node* next_;
  
public:
  node(DTYPE data);
  ~node();

  DTYPE get_data();
  void set_data(DTYPE& data);

  node* get_next();
  void set_next(node* next);

  friend ostream& operator<<(ostream& os, node&);
};

#endif 